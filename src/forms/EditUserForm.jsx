import React, { useState, useEffect } from 'react';

const EditUserForm = (props) => {

    useEffect(() => {
        setUser(props.currentUser)
    }, [props])

    const [user, setUser] = useState(props.currentUser);

    const handleChange = e => { //деструктурируем свойства объекта event.target.
        const { name, value } = e.target; //динамически устанавливаем ключи нашего объекта 
        setUser({ ...user, [name]: value });//на основе используемого поля ввода
    }

    const handleSubmit = e => {
        e.preventDefault();//предотвращаем обновление страницы по умолчанию
        if (user.name && user.username)
            props.updateUser(user);//проверяем, действительно ли были заполнены
    }

    return (
        <form>
            <label>Name</label>
            <input className="u-full-width" type="text" value={user.name} name="name" onChange={handleChange} />
            <label>Username</label>
            <input className="u-full-width" type="text" value={user.username} name="username" onChange={handleChange} />
            <button className="button-primary" type="submit" onClick={handleSubmit} >Edit user</button>
            <button type="submit" onClick={() => props.setEditing(false)} >Cancel</button>
        </form>
    )
}

export default EditUserForm;