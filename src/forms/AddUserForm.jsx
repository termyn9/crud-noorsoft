import React, { useState } from 'react';

const AddUserForm = (props) => {

    const initUser = { id: null, name: '', username: '' };

    const [user, setUser] = useState(initUser); //снова useState для управления состоянинм нового user

    const handleChange = e => {
        const { name, value } = e.target;//деструктуризация свойств объекта
        setUser({ ...user, [name]: value });//динамически устанавливаем ключи объекта 
    }                                    //на основе используемого поля ввода

    const handleSubmit = e => {
        e.preventDefault();  //когда состояние будет установлено для этого нового пользователя 
        if (user.name && user.username) { //передаем addUser функцию как обратный вызов после завершения handleChange. 
            handleChange(e, props.addUser(user)); //если добавляеть одного и того же пользователя друг за другом.
        }
    }

    return (
        <form>
            <label>Name</label>
            <input className="u-full-width" type="text" value={user.name} name="name" onChange={handleChange} />
            <label>Username</label>
            <input className="u-full-width" type="text" value={user.username} name="username" onChange={handleChange} />
            <button className="button-primary" type="submit" onClick={handleSubmit} >Add user</button>
        </form>
    )
}

export default AddUserForm;