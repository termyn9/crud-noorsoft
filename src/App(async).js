import React, { useState, useEffect } from "react";
import UserTable from "./tables/UserTable";
import AddUserForm from "./forms/AddUserForm";
import EditUserForm from "./forms/EditUserForm";
import { useAsyncRequest } from "./hooks"; 

//useState возвращает значение с состоянием и функцию для его обновления(setUsers)
//users - возвращаемое состояние
//совпадает со значением, переданным в качестве 1-го аргумента userList

const App = () => {
    const [data, loading] = useAsyncRequest(3); //подгружаем пользователей из data по фиксированному количеству = 3
    const [users, setUsers] = useState(null); //теперь нет списка userList, изначально users установлены по null



    //----------------Кусок с попыткой получить данные и впринципе взаимодействовать с API----------

    //запускается каждый раз, когда значения объектов в users изменяются 
    useEffect(() => { 
        if (data) { //если получили данные из API
          const formattedUsers = data.map((obj, i) => { //map - вывод результата выполнения по параметрам самого объекта obj и его id
            return { //при получении выводим внутренние параметры
              id: i,
              name: obj.name.first,
              username: obj.name.first + " " + obj.name.last,
            };
          });
          setUsers(formattedUsers); //инициилизируем пользователей
          //в соответствии с полученными данными
        }
      }, [data]); //хук вернул результат, а значит установили пользователей
       //которые были null в соответствии с полученными после setUsers(formattedUsers)
       //данные, полученные после useEffect не подходят usetTable
        //поэтому после форматируем результат setUsers(formattedUsers)

      const addUser = (user) => {//объект нового пользователя в массив пользовательских объектов
        user.id = users.length;
        setUsers([...users, user]);//текущий пользовательский массив неизменный, просто + 1 новый к нему
      };
    
      const deleteUser = (id) => {
        setUsers(users.filter((user) => user.id !== id));
      };//фильтр массива пользователей и пользователя 
      // с его id, которого надо удалить
      //setUsers для обновления состояния новых пользователей
    
    
      const [editing, setEditing] = useState(false);//исчпользуем ф-ию useState,чтобы проверить
      // редактируется ли пользователь в данный момент
    
      const initialUser = { id: null, name: "", username: "" };
    
      const [currentUser, setCurrentUser] = useState(initialUser);
    
      const editUser = (id, user) => {// функция редактирования
        setEditing(true);
        setCurrentUser(user);
      };
    
      const updateUser = (newUser) => {
        setUsers(
          users.map((user) => (user.id === currentUser.id ? newUser : user))
        );
        setCurrentUser(initialUser);
        setEditing(false);
      };
    
      return (
        <div className="container">
          <h1>React CRUD App with Hooks</h1>
          <div className="row">
            <div className="five columns">
              {editing ? (//если редактирование
                <div>
                  <h2>Edit user</h2>
                  <EditUserForm
                    currentUser={currentUser}//текущий пользователь
                    setEditing={setEditing}//установка изменений пользователя
                    updateUser={updateUser}//обновление пользователя
                  />
                </div>
              ) : (//добавление
                <div>
                  <h2>Add user</h2>
                  <AddUserForm addUser={addUser} />
                </div>
              )}
            </div>
            {loading || !users ? (//отображаем таблицу только
            // когда она не загружается и есть пользователи
              <p>Loading...</p>
            ) : (
              <div className="seven columns">
                <h2>List of users</h2>
    
                <UserTable
                  users={users}
                  deleteUser={deleteUser}//вызов функции
                  editUser={editUser}//вызов функции
                />
              </div>
            )}
          </div>
        </div>
      );
    };
    
    export default App;
    