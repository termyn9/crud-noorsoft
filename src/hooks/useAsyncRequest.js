import { useState, useEffect } from "react";

const useAsyncRequest = (amount) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => { //userEffect: получение данных из API при загрузке страницы
    //вызывается каждый раз, когда изменяется сумма объектов
    //в App(async) задали useAsyncRequest(3 - сумма объектов)
    //значит функция сработает 1 раз
    const fetchData = async () => { //fetch - интерфейс для запросов с сервера
        //try-catch обработчик ошибок для запроса async/await
      try {
        setLoading(true);
        const response = await fetch(
          `https://randomuser.me/api/?results=${amount}`
        );
        const json = await response.json();
        setData(json.results, setLoading(false));
      } catch (err) {
        console.warn("Something went wrong fetching the API...", err);
        setLoading(false);
      }
    };

    if (amount) {
      fetchData(amount);
    }
  }, [amount]);

  return [data, loading]; //2 переменные состояния - данные и загрузка
};

export default useAsyncRequest;
