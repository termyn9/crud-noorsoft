const data = [
    {
        id: 0,
        name: 'John',
        username: 'John Travolta'
    },
    {
        id: 1,
        name: 'Michael',
        username: 'Michael Jackson'
    },
    {
        id: 2,
        name: 'Isaac',
        username: 'Isaac Newton'
    }
]

export default data;